/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht7state;

/**
 *
 * @author aleks
 */
public class StatePatternDemo {
   public static void main(String[] args) {
      Context context = new Context();

      Charmander Charmander = new Charmander();
      Charmander.doAction(context);

      System.out.println(context.getState().toString());

      Charmeleon Charmeleon = new Charmeleon();
      Charmeleon.doAction(context);

      System.out.println(context.getState().toString());
      
      Charizard Charizard = new Charizard();
      Charizard.doAction(context);

      System.out.println(context.getState().toString());
   }
}