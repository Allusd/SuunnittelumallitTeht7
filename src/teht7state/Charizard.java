/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht7state;

/**
 *
 * @author aleks
 */
public class Charizard implements State {
   public void doAction(Context context) {
      System.out.println("Pokemon is in Charizard state");
      context.setState(this);	
   }

   public String toString(){
      return "Charizard State";
   }
}
